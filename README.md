# Benchmark :rocket:

- To make the test as clean as possible, we created a Docker container to isolate the tested server from the rest of the system. In addition to sandboxing the WSGI server, this ensured that every run started with a clean slate.

### Requirements :fire:

- install the requirements for testing the Gunicorn and the wsgi and CherryPy servers : 

```sh
pip install -r requirements.txt
```

## Server :rocket:

- Isolated in a Docker container.

- Allocated 2 CPU cores.

- Container’s RAM was capped at 512 MB.

## Testing :rocket:

- The servers were tested in a random order with an increasing number of simultaneous connections, ranging from 100 to 10,000.

- Each test lasted 30 seconds and was repeated 4 times.

## Conclusion :rocket:

- The benchmark’s results surprised us in a couple of different ways. First, we were blown away with Bjoern’s performance. However, we were also a bit suspicious at the discrepancy between it and the next highest performer. We need to investigate this further and would also love to hear your thoughts if you have any insight into our approach. Secondly, we were sorely disappointed with uWSGI. Either we misconfigured uWSGI, or the version we installed has some major bugs, but we’d also love to open this up for discussion.

- To summarize, here are some general insights that can be gleaned from the results of each server:

- [x] Bjoern: Appears to live up to its claim as a “screamingly fast, ultra-lightweight WSGI server.”

- [x] CherryPy: Fast performance, lightweight, and low errors. Not bad for pure Python.

- [x] Gunicorn: A good, consistent performer for medium loads.

- [x] Meinheld: Performs well and requires minimal resources. However, struggles at higher loads.

- [x] mod_wsgi: Integrates well into Apache and performs admirably.